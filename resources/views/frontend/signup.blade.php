

<!Doctype html>
<html>
<head>
     <meta charset="UTF-8">
     <title>Registration Form</title>
         <meta name="viewport" content="width=device-width, initial-scale=1">
         <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
         <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="//netdna.bootstrapcdn.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>

</head>
<style type="text/css">
	body{
	 background-image:url("{{ URL::asset('frontend/img/page-bg/1.jpg')}}");
	 
	 background-repeat:no-repeat;
	 background-size:cover;
	 width:100%;
	 height:100vh;
	 overflow:auto;
	 
}

/*-----for border----*/
.container{
	font-family:Roboto,sans-serif;
	  /* background-image:url('frontend\img\page-bg\1.jpg') ; */
    
     border-style: 1px solid grey;
     margin: 0 auto;
     text-align: center;
     opacity: 0.8;
     /*margin-top: 67px;*/
   box-shadow: 2px 5px 5px 0px #eee;
     max-width: 700px;
     /*padding-top: 10px;*/
     /*height: 363px;*/
     margin-top: 100px;
}
/*--- for label of first and last name---*/
.lastname{
	 margin-left: 51px;
     font-family: sans-serif;
     font-size: 14px;
     color: white;
     margin-top: 10px;
}
.firstname{
	 margin-left: 51px;
     font-family: sans-serif;
     font-size: 14px;
     color: white;
     margin-top: 5px;
}

.uid{
	 margin-left: 81px;
     font-family: sans-serif;
     font-size: 14px;
     color: white;
     margin-top: 5px;
}
.upload {
	/*margin-left: 1px;*/
     font-family: sans-serif;
     font-size: 14px;
     color: white;
     margin-top: 5px;
}
#lname{
	 margin-top:5px;
}
#phone{
	 margin-top:5px;
}
	  
#uid{
	 margin-top:5px;
	}
/*---for heading-----*/
.heading{
	 text-decoration:bold;
	 text-align : center;
	 font-size:30px;
	 color:white;
	 padding-top:10px;
}
/*-------for email----------*/
  /*------label----*/
#email{
	  margin-top: 5px;
}
/*-----------for Password--------*/
     /*-------label-----*/
.mail{
	 margin-left: 85px;
     font-family: sans-serif;
     color: white;
     font-size: 14px;
     margin-top: 13px;
}
.phone {
	margin-left: 61px;
     font-family: sans-serif;
     font-size: 14px;
     color: white;
     margin-top: 10px;
}
.pass{
	 color: white;
     margin-top: 9px;
     font-size: 14px;
     font-family: sans-serif;
     margin-left: 56px;
     margin-top: 9px;
}
#password{
 margin-top: 6px;
}

.cpass{
	 color: white;
     margin-top: 9px;
     font-size: 14px;
     font-family: sans-serif;
     margin-left: 6px;
     margin-top: 9px;
}
#cpassword{
 margin-top: 6px;
}
/*------------for phone Number--------*/
      /*----------label--------*/


/*--------------for Gender---------------*/
     /*--------------label---------*/
.gender {
	 color: white;
     font-family: sans-serif;
     font-size: 14px;
     margin-left: 78px;
     margin-top: 8px;
}

     /*---------- for Input type--------*/
.col-xs-2.male{
	 color: white;
     font-size: 13px;
     margin-top: 9px;
     padding-bottom: 16px;
}
.col-xs-6.female {
     color: white;
     font-size: 13px;
     margin-top: 9px;
     padding-bottom: 16px;
	 padding-right: 95px;
}	
/*------------For submit button---------*/
/*.sbutton{
	 color: white;
     font-size: 20px;
     border: 1px solid white;
     background-color: #080808;
     width: 32%;
     margin-left: 41%;
     margin-top: 16px;
	 box-shadow: 0px 2px 2px 0px white;
  	   
   }*/
.btn.btn-warning:hover {
    box-shadow: 2px 1px 2px 3px #99ccff;
	/*background:#5900a6;*/
	/*color:#fff;*/
	transition: background-color 1.15s ease-in-out,border-color 1.15s ease-in-out,box-shadow 1.15s ease-in-out;
	
}	 
	  
</style>
<body>

 <div class="container">
 	{{-- <form action="index.html" method="POST"> --}}
         <form class="login100-form validate-form" action="{{url('/actionsignup')}}" method="POST">
            {{@csrf_field()}}
 <!---heading---->
     <header class="heading"> Registration-Form</header><hr></hr>
	<!---Form starting----> 
	<div class="row ">

			
			
	
	 <!--- For Name---->
         <div class="col-sm-12">
             <div class="row">
				 
			     <div class="col-xs-4">
          	         <label class="firstname"> Name :</label> </div>
		         <div class="col-xs-8">
					 
					<input type="text" name="fname" id="fname" value="{{Request::old('fname')}}" placeholder="Enter your  Name" class="form-control " required>
					<FONT COLOR="RED">	@if ($errors->any())
							@if( $errors->has('fname'))
							
							{{ $errors->first('fname') }}
							</span>
						 @endif
						 @endif
							</FONT>	
				
				</div>
		      </div>
		 </div>
		 
		 
         {{-- <div class="col-sm-12">
		     <div class="row">
			     <div class="col-xs-4">
                     <label class="lastname">Last Name :</label></div>
				<div class ="col-xs-8">	 
		             <input type="text" name="lname" id="lname" placeholder="Enter your Last Name" class="form-control last" >
                </div>
		     </div>
		 </div> --}}
     <!-----For email---->
		 <div class="col-sm-12">
		     <div class="row">
			     <div class="col-xs-4">
		             <label class="mail" >Email :</label></div>
			     <div class="col-xs-8"	>	 
			          <input type="email" name="email"  id="email"placeholder="Enter your email" class="form-control">
				 
					  <FONT COLOR="RED">	@if ($errors->any())
							@if( $errors->has('email'))
							
							{{ $errors->first('email') }}
							</span>
						 @endif
						 @endif
							</FONT>
					
					</div>
		     </div>
		 </div>

		 <div class="col-sm-12">
		     <div class="row">
			     <div class="col-xs-4">
		             <label class="phone" >Phone No :</label></div>
			     <div class="col-xs-8"	>	 
			          <input type="text" name="phone"  id="phone" placeholder="Enter your phone no" class="form-control" >
					  <FONT COLOR="RED">	@if ($errors->any())
							@if( $errors->has('phone'))
							
							{{ $errors->first('phone') }}
							</span>
						 @endif
						 @endif
							</FONT>
				
					</div>
		     </div>
		 </div>

		 <div class="col-sm-12">
             <div class="row">
			     <div class="col-xs-4">
          	         <label class="uid">Userid :</label> </div>
		         <div class="col-xs-8">
		             <input type="text" name="uid" id="uid" placeholder="Enter your userid" class="form-control ">
					 <FONT COLOR="RED">	@if ($errors->any())
							@if( $errors->has('uid'))
							
							{{ $errors->first('uid') }}
							</span>
						 @endif
						 @endif
							</FONT>
			
					</div>
		      </div>
		 </div>
	 <!-----For Password and confirm password---->
          <div class="col-sm-12">
		        <div class="row">
				     <div class="col-xs-4">
		 	              <label class="pass">Password :</label>
		 	         </div>
				  <div class="col-xs-8">
			             <input type="password" name="password" id="password" placeholder="Enter your Password" class="form-control">
				 
						 <FONT COLOR="RED">	@if ($errors->any())
								@if( $errors->has('password'))
								
								{{ $errors->first('password') }}
								</span>
							 @endif
							 @endif
								</FONT>
						</div>
          		</div>
		  </div>

		  <div class="col-sm-12">
		        <div class="row">
				     <div class="col-xs-4">
		 	              <label class="cpass">Confirm Password :</label>
		 	         </div>
				  <div class="col-xs-8">
			             <input type="password" name="cpassword" id="cpassword" placeholder="Confirm your  Password" class="form-control">
					
						 <FONT COLOR="RED">	@if ($errors->any())
								@if( $errors->has('cpassword'))
								
								{{ $errors->first('cpassword') }}
								</span>
							 @endif
							 @endif
								</FONT>
					
						</div>
          		</div>
		  </div>
		  
     <!-----------For gender-------->
         <div class="col-sm-12">
		     <div class ="row">
                 <div class="col-xs-4 ">
			       <label class="gender">Gender:</label>
				 </div>
			 
			     <div class="col-xs-2 male text-left">	 
				     <input type="radio" name="gender"   value="male">Male</input>
				 </div>
				 
				 <div class="col-xs-6 female text-left">
				     <input type="radio"  name="gender"  value="female" >Female</input>
				 </div>
				 
				 <FONT COLOR="RED">	@if ($errors->any())
						@if( $errors->has('gender'))
						
						{{ $errors->first('gender') }}
						</span>
					 @endif
					 @endif
						</FONT>

			<div class="col-sm-12">
             <div class="row">
			     <div class="col-xs-4">
					   <label class="upload">Upload your image :</label> 
				 </div>
		         <div class="col-xs-8">
		             <input type="file" name="upload" id="upload" class="form-control ">
                 </div>
		     </div>
		 </div>&nbsp;
			
		  	 </div>
		     <div class="col-sm-12">
		         <button class="btn btn-primary">Submit</button>
		   </div>&nbsp;
		 </div>
	 </div>	 
		 		 
		</form> 
</div>

</body>		
</html>
	 
	 