@extends('layouts.layout')

@section('header')
@include('include/header')
@stop

@section('slide_menu')
@include('include/slide_menu')
@stop

@section('body')
@include($page)
@stop

@section('footer')
@include('include/footer')
@stop
