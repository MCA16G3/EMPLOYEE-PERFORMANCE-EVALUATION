@extends('backend.staff.layouts.layout')

@section('header')
@include('backend/admin/include/header')
@stop

@section('sidebar')
@include('backend/admin/include/sidebar')
@stop

@section('body')
@include('backend/admin/include/notification')
@include('backend/admin/'.$page)
@stop

@section('footer')
@include('backend/admin/include/footer')
@stop
