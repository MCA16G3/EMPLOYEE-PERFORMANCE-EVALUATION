

  <h1><center><font color="#3c8dbc">Administrative Work</font></center></h1>

  

<!-- Main content -->
<section class="content">
        <div class="row">

          
          <div class="col-xs-12">



            
                
            <div class="box">
              <div class="box-header">
                <h3 class="box-title">List oF ALL Staff</h3>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>No</th>
                    <th>UserName</th>
                    <th>Status</th>
                    
                  </tr>
                  </thead>
                  <tbody>
              @php
              $count=1;
              @endphp
              
@foreach($staff as $row)

                  <tr>
                  <td>{{$count}}</td>
                
                  <td>{{$row->username}}</td>
                    
                  <td>
                    @if($row->fk_status_id=='1')
                      {{'active'}}
                      @elseif($row->fk_status_id=='2')
                      {{'Block'}}
                      @endif
                  </td>
                    

                  

                  <td>
                    
                      @if($row->fk_status_id=='1')
                      <a href="{{url('admin/staff/block/update/'.$row->username)}}" class="">Block</a></td>
                 
                      @elseif($row->fk_status_id=='2')
                      <a href="{{url('admin/staff/active/update/'.$row->username)}}" class="">active</a></td>
                 
                      @endif
                    
                    
                  </tr>

                  @php
                  $count++;
                  @endphp
@endforeach

                  </tbody>
                  
                </table>
              </div>
              <!-- /.box-body -->
            </div>
            <!-- /.box -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </section>
      <!-- /.content -->