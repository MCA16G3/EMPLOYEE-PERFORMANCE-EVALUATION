<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
		<!-- sidebar: style can be found in sidebar.less -->
		<section class="sidebar">
		  <!-- Sidebar user panel -->
		  <div class="user-panel">
			<div class="pull-left image">
			 <br>
			</div>
			<div class="pull-left info">
			  <p><?=$username?></p>
			  <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
			</div>
		  </div>
		  <!-- search form -->
		 <br>
		  <!-- /.search form -->
		  <!-- sidebar menu: : style can be found in sidebar.less -->
		  <ul class="sidebar-menu" data-widget="tree">
			<li class="header">MAIN NAVIGATION</li>
			<li>
			<a href="{{url('/admin/dashboard')}}">
				<i class="fa fa-dashboard"></i> <span>Dashboard</span>
				
			  </a>
			  
			</li>

			
			<li>
			  <a href="{{url('/admin/AutoEvaluation')}}">
				<i class="fa fa-files-o"></i> <span>AutoEvaluation</span>
				
			  </a>
			  
			</li>
			
		
			<li class="treeview">
			  <a href="">
				<i class="fa fa-user"></i>
				<span>Staff</span>
				<span class="pull-right-container">
					  <i class="fa fa-angle-left pull-right"></i>
					</span>
			  </a>
			  <ul class="treeview-menu">
			
				<li><a href="{{url('admin/staff/list')}}"><i class="fa fa-circle-o"></i>Staffs List</a></li>
				
				<li><a href="{{url('admin/staff/active')}}"><i class="fa fa-circle-o"></i>Staffs active</a></li>
			
				<li><a href="{{url('admin/staff/block')}}"><i class="fa fa-circle-o"></i>Staffs block</a></li>
				
			  </ul>
			</li>

			<li>
			<a href="{{url('')}}">
				<i class="fa fa-sign-out"></i> <span>LogOut</span>
				
			  </a>
			  
			</li>

		  </ul>
		</section>
		<!-- /.sidebar -->
	  </aside>
	
	  <!-- Content Wrapper. Contains page content -->
	  <div class="content-wrapper">
		<!-- Content Header (Page header) -->
		<section class="content-header">
		  {{-- <h1> --}}
			{{-- Data Tables --}}
			{{-- <small>advanced tables</small> --}}
		  {{-- </h1> --}}
		  {{-- <ol class="breadcrumb"> --}}
			{{-- <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li> --}}
			{{-- <li><a href="#">Tables</a></li> --}}
			{{-- <li class="active">Data tables</li> --}}
			
			{{-- </ol> --}}
			

		</section>