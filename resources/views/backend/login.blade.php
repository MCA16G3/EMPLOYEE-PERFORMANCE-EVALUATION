<!DOCTYPE html>
<html lang="en">
<head>
	<title>Login</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="{{ URL::asset('backend/images/icons/favicon.ico')}}"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ URL::asset('backend/vendor/bootstrap/css/bootstrap.min.css')}}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ URL::asset('backend/fonts/font-awesome-4.7.0/css/font-awesome.min.css')}}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ URL::asset('backend/fonts/Linearicons-Free-v1.0.0/icon-font.min.css')}}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ URL::asset('backend/vendor/animate/animate.css')}}">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="{{ URL::asset('backend/vendor/css-hamburgers/hamburgers.min.css')}}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ URL::asset('backend/vendor/animsition/css/animsition.min.css')}}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ URL::asset('backend/vendor/select2/select2.min.css')}}">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="{{ URL::asset('backend/vendor/daterangepicker/daterangepicker.css')}}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ URL::asset('backend/css/util.css')}}">
	<link rel="stylesheet" type="text/css" href="{{ URL::asset('backend/css/main.css')}}">
<!--===============================================================================================-->
</head>
<body>
	


	<div class="limiter">
		<div class="container-login100" background="{{ URL::asset('frontend/img/courses/7.jpg')}}" style="
		background: url("{{ URL::asset('frontend/img/courses/7.jpg')}}");
		background-repeat:no-repeat;
		width: 100%;
		height: 100hv;
	">
			<div class="wrap-login100 p-l-55 p-r-55 p-t-65 p-b-50" background="{{ URL::asset('frontend/img/courses/7.jpg')}}">
            <form class="login100-form validate-form" action="{{url('actionlogin')}}" method="post">
                {{ csrf_field() }}
                
                <span class="login100-form-title p-b-33">
						 Login


						 @if(count($errors) > 0)
						 <div>
							<p style="color: red;">
								 @foreach($errors->all() as $error)
									 {{ $error }}
								 @endforeach
							</p>
						 </div>
					 @endif
						
					</span>

					<div class="wrap-input100 validate-input" data-validate = "userid is required">
						<input class="input100" type="text" name="userid" placeholder="Userid">
						<span class="focus-input100-1"></span>
						<span class="focus-input100-2"></span>
					</div>

					<div class="wrap-input100 rs1 validate-input" data-validate="Password is required">
						<input class="input100" type="password" name="pass" placeholder="Password">
						<span class="focus-input100-1"></span>
						<span class="focus-input100-2"></span>
					</div>

					<div class="container-login100-form-btn m-t-20">
						<button class="login100-form-btn">
							Sign in
						</button>
					</div>

					<div class="text-center p-t-45 p-b-4">
						<span class="txt1">
							Forgot
						</span>

						<a href="#" class="txt2 hov1">
							Username / Password?
						</a>
					</div>

					<div class="text-center">
						<span class="txt1">
							Create an account?
						</span>

						<a href={{url('/signup')}} class="txt2 hov1">
							Sign up
						</a>
					</div>
				</form>
			</div>
		</div>
	</div>
	

	
<!--===============================================================================================-->
	<script src="{{ URL::asset('backend/vendor/jquery/jquery-3.2.1.min.js')}}"></script>
<!--===============================================================================================-->
	<script src="{{ URL::asset('backend/vendor/animsition/js/animsition.min.js')}}"></script>
<!--===============================================================================================-->
	<script src="{{ URL::asset('backend/vendor/bootstrap/js/popper.js')}}"></script>
	<script src="{{ URL::asset('backend/vendor/bootstrap/js/bootstrap.min.js')}}"></script>
<!--===============================================================================================-->
	<script src="{{ URL::asset('backend/vendor/select2/select2.min.js')}}"></script>
<!--===============================================================================================-->
	<script src="{{ URL::asset('backend/vendor/daterangepicker/moment.min.js')}}"></script>
	<script src="{{ URL::asset('backend/vendor/daterangepicker/daterangepicker.js')}}"></script>
<!--===============================================================================================-->
	<script src="{{ URL::asset('backend/vendor/countdowntime/countdowntime.js')}}"></script>
<!--===============================================================================================-->
	<script src="{{ URL::asset('backend/js/main.js')}}"></script>

</body>
</html>