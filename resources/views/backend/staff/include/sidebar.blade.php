<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
		<!-- sidebar: style can be found in sidebar.less -->
		<section class="sidebar">
		  <!-- Sidebar user panel -->
		  <div class="user-panel">
			<div class="pull-left image">
			 <br>
			</div>
			<div class="pull-left info">
			  <p>Staff Name</p>
			  <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
			</div>
		  </div>
		  <!-- search form -->
		 <br>
		  <!-- /.search form -->
		  <!-- sidebar menu: : style can be found in sidebar.less -->
		  <ul class="sidebar-menu" data-widget="tree">
			<li class="header">MAIN NAVIGATION</li>
			<li>
			<a href="{{url('/staff/dashboard')}}">
				<i class="fa fa-dashboard"></i> <span>Dashboard</span>
				
			  </a>
			  
			</li>

			<li>
			  <a href="{{url('/staff/administrative')}}">
				<i class="fa fa-files-o"></i> <span>Administrative</span>
				
			  </a>
			  
			</li>

			
		
			<li class="treeview">
			  <a href="{{url('/staff/academicwork')}}">
				<i class="fa fa-user"></i>
				<span>Academic Activities</span>
				<span class="pull-right-container">
					  <i class="fa fa-angle-left pull-right"></i>
					</span>
			  </a>
			  <ul class="treeview-menu">
				<li><a href="{{url('staff/qualification')}}"><i class="fa fa-circle-o"></i> Details of Qualification</a></li>
				<li><a href="{{url('staff/instructionalwork')}}"><i class="fa fa-circle-o"></i> Instructional Work</a></li>
				<li><a href="{{url('staff/participation')}}"><i class="fa fa-circle-o"></i> Participations</a></li>
				<li><a href="{{url('staff/activitiesorganized')}}"><i class="fa fa-circle-o"></i> Activities Organized</a></li>
				<li><a href="{{url('staff/industrialtraining')}}"><i class="fa fa-circle-o"></i> Industrial Training</a></li>
				<li><a href="{{url('staff/Curricular')}}"><i class="fa fa-circle-o"></i> Curricular & co-curricular</a></li>
				<li><a href="{{url('staff/extracurricular')}}"><i class="fa fa-circle-o"></i> Extra Curricular</a></li>
				<li><a href="{{url('staff/supervisorywork')}}"><i class="fa fa-circle-o"></i> Supervisory work</a></li>
				<li><a href="{{url('staff/speciallectures')}}"><i class="fa fa-circle-o"></i> Special Lecture delivered</a></li>

			  </ul>
			</li>


			<li class="treeview">
			  <a href="{{url('/staff/research')}}">
				<i class="fa fa-laptop"></i>
				<span>Research</span>
				<span class="pull-right-container">
					  <i class="fa fa-angle-left pull-right"></i>
					</span>
			  </a>
			  <ul class="treeview-menu">
				<li><a href="{{url('staff/development')}}"><i class="fa fa-circle-o"></i> Development</a></li>
				<li><a href="{{url('staff/fundgeneration')}}"><i class="fa fa-circle-o"></i> Fund generation activities</a></li>
				<li><a href="{{url('staff/researchguidance')}}"><i class="fa fa-circle-o"></i> Research Guidance</a></li>
				<li><a href="{{url('staff/researchpapers')}}"><i class="fa fa-circle-o"></i>Research papers</a></li>
				<li><a href="{{url('staff/publicationofbooks')}}"><i class="fa fa-circle-o"></i> Publication of Books</a></li>
				<li><a href="{{url('staff/professionalsociety')}}"><i class="fa fa-circle-o"></i> Professional society</a></li>
				<li><a href="{{url('staff/achievements')}}"><i class="fa fa-circle-o"></i> Achievements & awards</a></li>
				
			  </ul>
			</li>
		
			<li>
			  <a href="{{url('staff/servicetocommunity')}}">
				<i class="fa fa-pie-chart"></i> <span>Service to Community</span>
				
			  </a>
			  
			</li>

			<li>
			<a href="{{url('staff/otheractivities')}}">
				<i class="fa fa-book"></i> <span>Other Activities</span>
				
			  </a>
			  
			</li>

			<li>
			<a href="{{url('')}}">
				<i class="fa fa-sign-out"></i> <span>LogOut</span>
				
			  </a>
			  
			</li>

		  </ul>
		</section>
		<!-- /.sidebar -->
	  </aside>
	
	  <!-- Content Wrapper. Contains page content -->
	  <div class="content-wrapper">
		<!-- Content Header (Page header) -->
		<section class="content-header">
		  {{-- <h1> --}}
			{{-- Data Tables --}}
			{{-- <small>advanced tables</small> --}}
		  {{-- </h1> --}}
		  {{-- <ol class="breadcrumb"> --}}
			{{-- <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li> --}}
			{{-- <li><a href="#">Tables</a></li> --}}
			{{-- <li class="active">Data tables</li> --}}
			
			{{-- </ol> --}}
			

		</section>