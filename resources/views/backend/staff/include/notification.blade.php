
@if(Session::has('success')||Session::has('error'))

    @if(Session::has('success'))

    <div class="col-md-12">
        <div class="box box-default">
          <div class="box-header with-border">
            <i class="fa fa-bullhorn"></i>
  
            <h3 class="box-title">Callouts</h3>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            
                       
            <div class="callout callout-success">
            <h4>{{Session::get('success')}}</h4>
  
  
            </div>
              
          </div>
          <!-- /.box-body -->
        </div>
  

   
    </div>
    
    @endif


@endif