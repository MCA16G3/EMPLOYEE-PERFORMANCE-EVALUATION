<div class="box-body">
        
    <form action="{{url('staff/supervisorywork/insert')}}" method="POST">

        {{@csrf_field()}}
		
    <div class="col-md-12 table-responsive">
            <h1><center><font color="#3c8dbc">Academic Supervisory Work(UG & PG Project Guidance)</font></center></h1>
        <table class="table table-bordered table-hover table-sortable table-container col-sm-12" id="tab_logic">
            <thead>
                <tr>
                    <th class="text-center col">
                        Degree
                    </th>
                    <th class="text-center col">
                        Name(s) of student(s)
                    </th>
                    <th class="text-center col">
                        Co-guide if any
                    </th>
                    <th class="text-center col">
                        Title of the project
                    </th>
                    <th class="text-center col">
                        Current status
                    </th>
                  
                </tr>
                
            </thead>
            <tbody id="addnewrow">
                
                <tr >

                    @foreach($supervisorywork as $row)

                     
                        
                        <td data-name="details">
                            {{$row->degree}}
                        </td>
                        <td data-name="period">
                                {{$row->name_std}}
                        </td>
                        <td data-name="specific">
                                {{$row->co_guide}}
                        </td>
                        <td data-name="specific">
                                {{$row->ttl_prj}}
                        </td>
                        <td data-name="specific">
                                {{$row->cur_sts}}
                        </td>
                        
                        <td data-name="del">
                        <a class="btn btn-danger glyphicon glyphicon-remove row-remove" href="{{url('staff/supervisorywork/delete/'.$row->id)}}"></a>
                        </td>
                    </tr>

                    @endforeach
                
        


</tbody>
            
        </table>
        <a id="add_row" class="btn btn-default pull-right">Add Row</a>
    </div>
    
    <div class="userinput row">
            <center>
                <input type="submit" value="Save" class="btn btn-primary">
            </center>
    
    
        </div>
            </div>

            
    

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js">	</script>	


<script>
$(document).ready(function () {
var i=0;
$('#add_row').click(function (e) { 


i++;

$('#addnewrow').append("<tr  id='removeitem"+i+"'><td data-name='details'><input type='text' name='name[]' placeholder='degree' class='form-control'></td><td data-name='period'><input type='text' name='name1[]' placeholder='names of students' class='form-control'></td><td data-name='specific'><input type='text' name='name2[]' placeholder='Co-guide if any' class='form-control'></td><td data-name='specific'><input type='text' name='name3[]' placeholder='title ofthe project' class='form-control'></td><td data-name='specific'><input type='text' name='name4[]' placeholder='Current status' class='form-control'></td><td data-name='del'><button name='del0' class='btn btn-danger glyphicon glyphicon-remove row-remove' value='' name='del1'></button></td></tr>");

});

  
});


function Remove(id)
{


$("#removeitem"+id).remove();

}

</script>

    
    </div>
    <!-- /.box-body -->