<div class="box-body">
        
		<form action="{{url('staff/participation/insert')}}" method="POST">

			{{@csrf_field()}}
		
		<div class="col-md-12 table-responsive">
				<h1><center><font color="#3c8dbc">Participation in STTP,Workshop,Seminar,Symposium,Conference Training etc</font></center></h1>
			<table class="table table-bordered table-hover table-sortable table-container col-sm-12" id="tab_logic">
				<thead>
					<tr>
						<th class="text-center col" rowspan="2">
							Nature of activity
						</th>
						<th class="text-center col" rowspan="2">
							organized by
						</th>
						<th class="text-center col" colspan="2">
							Duration
						</th>
    					
                    </tr>
                    <tr>
                            <th>From</th>
                            <th>To</th>
                    </tr>
					
				</thead>
				<tbody id="addnewrow">

						<tr >

								@foreach($participation as $row)
		
								 
									
									<td data-name="details">
										{{$row->nature_activity}}
									</td>
									<td data-name="period">
											{{$row->org_by}}
									</td>
									<td data-name="specific">
											{{$row->dur_from}}
									</td>
									<td data-name="specific">
											{{$row->dur_to}}
								
									<td data-name="del">
									<a class="btn btn-danger glyphicon glyphicon-remove row-remove" href="{{url('staff/participation/delete/'.$row->id)}}"></a>
									</td>
								</tr>
		
								@endforeach
		


    				
                       
                    </tr>
                    <tr>

                    </tr>
					
					
			


	</tbody>
	
			
				
			
			

		
			</table>
			<a id="add_row" class="btn btn-default pull-right">Add Row</a>
		</div>
		
		<div class="userinput row">
				<center>
					<input type="submit" value="Save" class="btn btn-primary">
				</center>
		
		
			</div>
				</div>
		
	
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js">	</script>	
    
    
<script>
    $(document).ready(function () {
  var i=0;
 $('#add_row').click(function (e) { 


   i++;

  $('#addnewrow').append("<tr  id='removeitem"+i+"'><td data-name='details'><input type='text' name='name[]' placeholder='Nature of activity' class='form-control'></td><td data-name='period'><input type='text' name='name1[]' placeholder='Organized by' class='form-control'></td><td data-name='specific'><input type='date' name='name2[]' placeholder='From' class='form-control'></td><td data-name='specific'><input type='date' name='name3[]' placeholder='To' class='form-control'></td><td data-name='del'><button name='del0' class='btn btn-danger glyphicon glyphicon-remove row-remove' value='' name='del1' onclick='Remove("+i+")'></button></td></tr>");

 });
  
      
});


function Remove(id)
{


$("#removeitem"+id).remove();

}
    </script>
    
		
        </div>
        <!-- /.box-body -->