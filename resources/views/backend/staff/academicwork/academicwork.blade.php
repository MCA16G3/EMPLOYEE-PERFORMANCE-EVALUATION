<div class="box-body">
        
    <form action="{{url('staff/academicwork/insert')}}" method="POST">

        {{@csrf_field()}}
    
    <div class="col-md-12 table-responsive">
            <h1><center><font color="#3c8dbc">Details Of Qualification Up-gradation</font></center></h1>
        <table class="table table-bordered table-hover table-sortable table-container col-sm-12" id="tab_logic">
            <thead>
                <tr>
                    <th class="text-center col">
                        Degree
                    </th>
                    <th class="text-center col">
                        FT / PT / Distance
                    </th>
                    <th class="text-center col">
                        College / University
                    </th>
                    
                    <th class="text-center col">
                        Date of joining
                    </th>

                    <th class="text-center col">
                        Current Status
                    </th>
                </tr>
               
                
            </thead>
            <tbody id="addnewrow">

                    <tr >

                            @foreach($academicwork as $row)
    
                            

                                <td data-name="details">
                                    {{$row->degree}}
                                </td>
                                <td data-name="period">
                                        {{$row->ft_pt_dist}}
                                </td>
                                <td data-name="specific">
                                        {{$row->clg_uni}}
                                </td>
                                <td data-name="specific">
                                        {{$row->date_join}}
                                </td>
                             <td data-name="specific">
                                        {{$row->cur_sts}}
                                <td data-name="del">
                                <a class="btn btn-danger glyphicon glyphicon-remove row-remove" href="{{url('staff/academicwork/delete/'.$row->id)}}"></a>
                                </td>
                            </tr>
    
                            @endforeach
    
                </tr>
        
</tbody>

        </table>
        <a id="add_row" class="btn btn-default pull-right">Add Row</a>
    </div>
    
    <div class="userinput row">
            <center>
                <input type="submit" value="Save" class="btn btn-primary">
            </center>
    
        </div>
            </div>
    

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js">	</script>	


<script>
$(document).ready(function () {
var i=0;
$('#add_row').click(function (e) { 


i++;

$('#addnewrow').append("<tr  id='removeitem"+i+"'><td data-name='details'><input type='text' name='name[]' placeholder='Degree' class='form-control'></td><td data-name='period'><input type='text' name='name1[]' placeholder='FT / PT / Distance' class='form-control'></td><td data-name='specific'><input type='text' name='name2[]' placeholder='College / University' class='form-control'></td><td data-name='specific'><input type='date' name='name3[]' placeholder='Date of joining' class='form-control'></td><td data-name='specific'><input type='text' name='name4[]' placeholder='Current Status' class='form-control'></td><td data-name='del'><button name='del0' class='btn btn-danger glyphicon glyphicon-remove row-remove' value='' name='del1' onclick='Remove("+i+")'></button></td></tr>");

});

  
});


function Remove(id)
{


$("#removeitem"+id).remove();

}
</script>

    
    </div>
    <!-- /.box-body -->