<div class="box-body">
        
		
<form action="{{url('staff/instructionalwork/insert')}}" method="POST">

        {{@csrf_field()}}

    <div class="col-md-12 table-responsive">
        <h1><center><font color="#3c8dbc">Instructional Work(odd & even semesters data)</font></center></h1>
        <table class="table table-bordered table-hover table-sortable table-container col-sm-12" id="tab_logic">
            <thead>
                <tr  bordercolor="red">
                    <th class="text-center col" rowspan="2">
                        Semester & programme
                    </th>
                    <th class="text-center col" rowspan="2">
                       Course title
                    </th>
                    <th class="text-center col" colspan="3">
                        Hours / week
                    </th>
                    <th class="text-center col" rowspan="2">
                       Class / batch strength
                    </th>
                    <th class="text-center col" colspan="2">
                        Test result
                    </th>
                    <th class="text-center col" rowspan="2">
                        Exam pass percentage
                    </th>
                </tr>
                <tr>
                    <th>L</th>
                    <th>T</th>
                    <th>P</th>
                    <th>overall average</th>
                    <th>number got < 50%</th>
                </tr>
                
                
                
            </thead>
            <tbody id="addnewrow">


                    <tr >

                        @foreach($instructionalwork as $row)

                         
                            
                            <td data-name="details">
                                {{$row->sem_prg}}
                            </td>
                            <td data-name="period">
                                    {{$row->cr_title}}
                            </td>
                            <td data-name="specific">
                                    {{$row->L}}
                            </td>
                            <td data-name="specific">
                                    {{$row->T}}
                            </td>
                            <td data-name="specific">
                                    {{$row->P}}
                            </td>
                            <td data-name="specific">
                                    {{$row->class}}
                            </td>
                            <td data-name="specific">
                                    {{$row->overallavg}}
                            </td>
                            <td data-name="specific">
                                    {{$row->numbergot}}
                            </td>
                            <td data-name="specific">
                                    {{$row->exampass}}
                            </td>
                            <td data-name="del">
                            <a class="btn btn-danger glyphicon glyphicon-remove row-remove" href="{{url('staff/instructionalwork/delete/'.$row->id)}}"></a>
                            </td>
                        </tr>

                        @endforeach



                

                
            
            
            </tbody>
            
        </table>
<a id="add_row" class="btn btn-default pull-right">Add Row</a>
</div>

<div class="userinput row">
        <center>
            <input type="submit" value="Save" class="btn btn-primary">
        </center>


    </div>
        </div>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js">	</script>	
    
    
<script>
    $(document).ready(function () {
  var i=0;
 $('#add_row').click(function (e) { 


   i++;

  $('#addnewrow').append("<tr  id='removeitem"+i+"'><td data-name='details'><input type='text' name='name[]' placeholder='Semester & programme' class='form-control'></td><td data-name='period'><input type='text' name='name1[]' placeholder='Course title' class='form-control'></td><td data-name='specific'><input type='text' name='name2[]' placeholder='L' class='form-control'></td><td data-name='specific'><input type='text' name='name3[]' placeholder='T' class='form-control'></td><td data-name='specific'><input type='text' name='name4[]' placeholder='P' class='form-control'></td><td data-name='specific'><input type='text' name='name5[]' placeholder='Class / batch strength' class='form-control'><td data-name='pecific'><input type='text' name='name6[]' placeholder='Overall avrg' class='form-control'></td><td data-name='specific'><input type='text' name='name7[]' placeholder='Number got<50%' class='form-control'></td></td><td data-name='specific'><input type='text' name='name8[]' placeholder='Exam pass percentage' class='form-control'></td><td data-name='del'><button name='del0' class='btn btn-danger glyphicon glyphicon-remove row-remove'  name='del1' onclick='Remove("+i+")'></button></td></tr>");

 });
  
      
});


function Remove(id)
{


$("#removeitem"+id).remove();

}
    </script>
    
    </div>