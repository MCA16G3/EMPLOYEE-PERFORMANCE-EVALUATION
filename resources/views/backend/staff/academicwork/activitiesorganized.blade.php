
<div class="box-body">
        
    <form action="{{url('staff/activitiesorganized/insert')}}" method="POST">

        {{@csrf_field()}}
		
        <div class="col-md-12 table-responsive">
                <h1><center><font color="#3c8dbc">Activities Organized(Workshop/Symposium/Seminar/FDP/STTP/Conference etc)</font></center></h1>
            <table class="table table-bordered table-hover table-sortable table-container col-sm-12" id="tab_logic">
                <thead>
                    <tr>
                        <th class="text-center col" rowspan="2">
                            Activity
                        </th>
                        <th class="text-center col" rowspan="2">
                           Title of activity
                        </th>
                        <th class="text-center col" colspan="2">
                            Duration
                        </th>
                        <th class="text-center col" rowspan="2">
                           Local / national/ international
                        </th>
                        <th class="text-center col" rowspan="2">
                            No.of participants
                        </th>
                        <th class="text-center col" rowspan="2">
                            Major sponsors, if any
                        </th>


                        
                 </tr>
                    <tr>
                        <th>From</th>
                        <th>To</th>
                    
                        
                    </tr>
                    
                </thead>
                <tbody id="addnewrow">
                   
                    <tr >

                        @foreach($activitiesorganized as $row)

                         
                            
                            <td data-name="details">
                                {{$row->activity}}
                            </td>
                            <td data-name="period">
                                    {{$row->title_activity}}
                            </td>
                            <td data-name="specific">
                                    {{$row->dur_from}}
                            </td>
                            <td data-name="specific">
                                    {{$row->dur_to }}
                            </td>
                            <td data-name="specific">
                                    {{$row->loc_nat_int}}
                            </td>
                            <td data-name="specific">
                                    {{$row->no_part}}
                            </td>
                            <td data-name="specific">
                                    {{$row->spons}}
                            </td>
                           
                            <td data-name="del">
                            <a class="btn btn-danger glyphicon glyphicon-remove row-remove" href="{{url('staff/activitiesorganized/delete/'.$row->id)}}"></a>
                            </td>
                        </tr>

                        @endforeach 
                
                
                </tbody>
                
            </table>
            <a id="add_row" class="btn btn-default pull-right">Add Row</a>
        </div>
        
        <div class="userinput row">
                <center>
                    <input type="submit" value="Save" class="btn btn-primary">
                </center>
        
        
            </div>
                </div>
        
    
    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js">	</script>	
        
        
    <script>
        $(document).ready(function () {
      var i=0;
     $('#add_row').click(function (e) { 
    
    
       i++;
    
      $('#addnewrow').append("<tr id='add"+i+"' data-id='1'><td data-name='details'><input type='text' name='name[]' placeholder=' Activity' class='form-control'></td><td data-name='period'><input type='text' name='name1[]' placeholder='Title of activity' class='form-control'></td><td data-name='specific'><input type='text' name='name2[]' placeholder='From' class='form-control'></td><td data-name='specific'><input type='text' name='name3[] placeholder='To' class='form-control'></td><td data-name='specific'><input type='text' name='name4[]' placeholder='Local / national / international' class='form-control'></td><td data-name='specific'><input type='text' name='name5[]' placeholder='No.of participants' class='form-control'></td> <td data-name='specific'><input type='text' name='name6[]' placeholder='Major sponsors, if any' class='form-control'></td><td data-name='del'><button name='del0' class='btn btn-danger glyphicon glyphicon-remove row-remove' value='' name='del1'></button></td></tr>");
    
     });
      
          
    });
    
    
    function Remove(id)
{


$("#removeitem"+id).remove();

}
        </script>
        
        </div>