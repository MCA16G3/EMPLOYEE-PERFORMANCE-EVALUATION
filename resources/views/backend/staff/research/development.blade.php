<div class="box-body">
        
			
<form action="{{url('staff/development/insert')}}" method="POST">

    {{@csrf_field()}}

		
    <div class="col-md-12 table-responsive">
            <h1><center><font color="#3c8dbc">Development of New Department Laboratory/Center/Experimental Setup etc</font></center></h1>
        <table class="table table-bordered table-hover table-sortable table-container col-sm-12" id="tab_logic">
            <thead>
                <tr>
                    <th class="text-center col">
                        Details of activity
                    </th>
                    <th class="text-center col">
                        Nature of work
                    </th>
                    <th class="text-center col">
                        Current status
                    </th>
                   
                </tr>
                
            </thead>
            <tbody id="addnewrow">
                
                <tr >

                    @foreach($development as $row)

                     
                        
                        <td data-name="details">
                            {{$row->det_act}}
                        </td>
                        <td data-name="period">
                                {{$row->nat_wrk}}
                        </td>
                        <td data-name="specific">
                                {{$row->crnt_sts}}
                        </td>
                     
                       
                        <td data-name="del">
                        <a class="btn btn-danger glyphicon glyphicon-remove row-remove" href="{{url('staff/development/delete/'.$row->id)}}"></a>
                        </td>
                    </tr>

                    @endforeach

        


</tbody>
            
        </table>
        <a id="add_row" class="btn btn-default pull-right">Add Row</a>
    </div>
    
    <div class="userinput row">
            <center>
                <input type="submit" value="Save" class="btn btn-primary">
            </center>
    
    
        </div>
            </div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js">	</script>	


<script>
$(document).ready(function () {
var i=0;
$('#add_row').click(function (e) { 


i++;

$('#addnewrow').append("<tr  id='removeitem"+i+"'><td data-name='details'><input type='text' name='name[]' placeholder='Details of activity' class='form-control'></td><td data-name='period'><input type='text' name='name1[]' placeholder='Nature of work' class='form-control'></td><td data-name='specific'><input type='text' name='name2[]' placeholder=' Current status' class='form-control'></td><td data-name='del'><button name='del0' class='btn btn-danger glyphicon glyphicon-remove row-remove' value='' name='del1'></button></td></tr>");

});

  
});


function Remove(id)
{


$("#removeitem"+id).remove();

}
</script>

    
    </div>
    <!-- /.box-body -->