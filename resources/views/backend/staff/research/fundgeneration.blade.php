<div class="box-body">
        
        <form action="{{url('staff/fundgeneration/insert')}}" method="POST">

            {{@csrf_field()}}
		
    <div class="col-md-12 table-responsive">
            <h1><center><font color="#3c8dbc">Fund Generation Activities(Proposal to Funding Agencies,Testing,Consultancy etc)</font></center></h1>
        <table class="table table-bordered table-hover table-sortable table-container col-sm-12" id="tab_logic">
            <thead>
                <tr>
                    <th class="text-center col">
                        Nature of activity
                    </th>
                    <th class="text-center col">
                        Concerned agency
                    </th>
                    <th class="text-center col">
                        Fund generated /applied / sanctioned
                    </th>
                    <th class="text-center col">
                        Current status of activity
                    </th>
                    
                    
                </tr>
                
            </thead>
            <tbody id="addnewrow">
                <tr >

                    @foreach($fundgeneration as $row)

                     
                        
                        <td data-name="details">
                            {{$row->nat_actvty}}
                        </td>
                        <td data-name="period">
                                {{$row->conc_agncy}}
                        </td>
                        <td data-name="specific">
                                {{$row->fund_gen}}
                        </td>
                        <td data-name="specific">
                                {{$row->act_crnt_sts }}
                        </td>
                       
                        <td data-name="del">
                        <a class="btn btn-danger glyphicon glyphicon-remove row-remove" href="{{url('staff/fundgeneration/delete/'.$row->id)}}"></a>
                        </td>
                    </tr>

                    @endforeach  
        
                
                
        


</tbody>
            
        </table>
        <a id="add_row" class="btn btn-default pull-right">Add Row</a>
    </div>
    
    <div class="userinput row">
            <center>
                <input type="submit" value="Save" class="btn btn-primary">
            </center>
    
    
        </div>
            </div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js">	</script>	


<script>
$(document).ready(function () {
var i=0;
$('#add_row').click(function (e) { 


i++;

$('#addnewrow').append("<tr id='removeitem"+i+"'><td data-name='details'><input type='text' name=name[]' placeholder='Nature of activity' class='form-control'></td><td data-name='period'><input type='text' name='name1[]' placeholder='Concerned agency' class='form-control'></td><td data-name='specific'><input type='text' name='name2[]' placeholder='Fund generated /applied / sanctioned' class='form-control'></td><td data-name='specific'><input type='text' name='name3[]' placeholder='Currnt status of activity' class='form-control'></td><td data-name='del'><button name='del0' class='btn btn-danger glyphicon glyphicon-remove row-remove' value='' name='del1'></button></td></tr>");

});

  
});



function Remove(id)
{


$("#removeitem"+id).remove();

}
</script>

    
    </div>
    <!-- /.box-body -->