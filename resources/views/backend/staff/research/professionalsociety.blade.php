<div class="box-body">
        


				
<form action="{{url('staff/professionalsociety/insert')}}" method="POST">

        {{@csrf_field()}}

		
        <div class="col-md-12 table-responsive">
                <h1><center><font color="#3c8dbc">Details of professional society activities</font></center></h1>
            <table class="table table-bordered table-hover table-sortable table-container col-sm-12" id="tab_logic">
                <thead>
                    <tr>
                        <th class="text-center col">
                            Details of professional society activities
                        </th>
                        
                         {{-- <th class="text-center" style="border-top: 1px solid #ffffff; border-right: 1px solid #ffffff;"> 
                        </th>  --}}
                    </tr>
                    
                </thead>
                <tbody id="addnewrow">
                    <tr id="addr0" data-id="0" class="hidden">
                        <td data-name="details">
                            <input type="textarea" name="details0" placeholder="Details of professional society activities" class="form-control">
                        </td>
                      
                        <td data-name="del">
                            <button nam"del0"="" class="btn btn-danger glyphicon glyphicon-remove row-remove"></button>
                        </td>
                    </tr>
                    
    
                    
                    @foreach($professionalsociety as $row)

                         
                            
                            <td data-name="details">
                                {{$row->details}}
                            </td>
                            <td data-name="del">
                            <a class="btn btn-danger glyphicon glyphicon-remove row-remove" href="{{url('staff/professionalsociety/delete/'.$row->id)}}"></a>
                            </td>
                        </tr>

                        @endforeach




            
    
    
    </tbody>
                
            </table>
    <a id="add_row" class="btn btn-default pull-right">Add Row</a>
        </div>
        
    



        <div class="userinput row">
            <center>
                <input type="submit" value="Save" class="btn btn-primary">
            </center>
    
    
        </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js">	</script>	
    
    
    <script>
    $(document).ready(function () {
    var i=0;
    $('#add_row').click(function (e) { 
    
    
    i++;
    
    $('#addnewrow').append("<tr id='add"+i+"' data-id='1'><td data-name='details'><input type='textarea' name='name[]' placeholder='Details of professional society activities' class='form-control'></td><td data-name='del'><button name='del0' class='btn btn-danger glyphicon glyphicon-remove row-remove' value='' name='del1'></button></td></tr>");
    
    });
    
      
    });
    
    
    function Remove(id)
    {
    
    $('#'+id).remove();
    }
    </script>
    
        
        </div>
        <!-- /.box-body -->