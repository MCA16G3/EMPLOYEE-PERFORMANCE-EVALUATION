<div class="box-body">
        
    <form action="{{url('staff/researchguidance/insert')}}" method="POST">

        {{@csrf_field()}}
		
    <div class="col-md-12 table-responsive">
            <h1><center><font color="#3c8dbc">Research Guidance</font></center></h1>
        <table class="table table-bordered table-hover table-sortable table-container col-sm-12" id="tab_logic">
            <thead>
                <tr>
                    <th class="text-center col">
                        Ms / M.phil
                    </th>
                    <th class="text-center col">
                        Name of student
                    </th>
                    <th class="text-center col">
                        Date of registration
                    </th>
                    <th class="text-center col">
                        FT / PT
                    </th>
                    <th class="text-center col">
                        Co-guide, if any
                    </th>
                    <th class="text-center col">
                        Current status
                    </th>
                    
                </tr>
                
            </thead>
            <tbody id="addnewrow">
                <tr>
                

                    @foreach($researchguidance as $row)

                     
                        
                    <td data-name="details">
                        {{$row->ms_mphil}}
                    </td>
                    <td data-name="period">
                            {{$row->name_std}}
                    </td>
                    <td data-name="specific">
                            {{$row->reg_date}}
                    </td>
                    
                    <td data-name="specific">
                            {{$row->ft_pt }}
                    </td>
                    
                    <td data-name="specific">
                            {{$row->co_guide }}
                    </td>
                    <td data-name="specific">
                            {{$row->crt_sts }}
                    </td>

                   
                    <td data-name="del">
                    <a class="btn btn-danger glyphicon glyphicon-remove row-remove" href="{{url('staff/researchguidance/delete/'.$row->id)}}"></a>
                    </td>
                </tr>

                @endforeach  
               
                
        


</tbody>
            
        </table>
        
        <a id="add_row" class="btn btn-default pull-right">Add Row</a>
    </div>
    
    <div class="userinput row">
            <center>
                <input type="submit" value="Save" class="btn btn-primary">
            </center>
    
    
        </div>
            </div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js">	</script>	


<script>
$(document).ready(function () {
var i=0;
$('#add_row').click(function (e) { 


i++;

$('#addnewrow').append("<tr  id='removeitem"+i+"'><td data-name='details'><input type='text' name='name[]' placeholder=' Ms / M.phil' class='form-control'></td><td data-name='period'><input type='text' name='name1[]' placeholder='Name of student' class='form-control'></td><td data-name='specific'><input type='text' name='name2[]' placeholder='Date of registration' class='form-control'></td><td data-name='specific'><input type='text' name='name3[]' placeholder='FT/PT' class='form-control'></td><td data-name='specific'><input type='text' name='name4[]' placeholder='Co-guide,if any' class='form-control'></td><td data-name='specific'><input type='text' name='name5[]' placeholder='Current status' class='form-control'></td><td data-name='del'><button name='del0' class='btn btn-danger glyphicon glyphicon-remove row-remove' value='' name='del1'></button></td></tr>");

});

  
});


function Remove(id)
{


$("#removeitem"+id).remove();

}
</script>

    
    </div>
    <!-- /.box-body -->