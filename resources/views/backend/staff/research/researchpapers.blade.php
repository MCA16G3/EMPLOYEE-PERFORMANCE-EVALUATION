<div class="box-body">

        <form action="{{url('staff/researchpapers/insert')}}" method="POST">

            {{@csrf_field()}}
		
		
    <div class="col-md-12 table-responsive">
            <h1><center><font color="#3c8dbc">Research Papers Published in Journals/Conferences)</font></center></h1>
        <table class="table table-bordered table-hover table-sortable table-container col-sm-12" id="tab_logic">
            <thead>
                <tr>
                    <th class="text-center col">
                        Title of the paper
                    </th>
                    <th class="text-center col">
                        Co-author(s) if any
                    </th>
                    <th class="text-center col">
                        Name of the journal / conference/publisher
                    </th>
                    <th class="text-center col">
                        Vol & no
                    </th>
                    <th class="text-center col">
                        Month & year
                    </th>
                    
                    
                </tr>
                
            </thead>
            <tbody id="addnewrow">
               
                <tr>
                

                    @foreach($researchpapers as $row)

                     
                        
                    <td data-name="details">
                        {{$row->tlt_ppr}}
                    </td>
                    <td data-name="period">
                            {{$row->co_auth}}
                    </td>
                    <td data-name="specific">
                            {{$row->name_jrnl}}
                    </td>
                    
                    <td data-name="specific">
                            {{$row->vol_no }}
                    </td>
                    
                    <td data-name="specific">
                            {{$row->mnth_yr }}
                    </td>
                    

                   
                    <td data-name="del">
                    <a class="btn btn-danger glyphicon glyphicon-remove row-remove" href="{{url('staff/researchpapers/delete/'.$row->id)}}"></a>
                    </td>
                </tr>

                @endforeach  
                
                
        


</tbody>
            
        </table>
        <a id="add_row" class="btn btn-default pull-right">Add Row</a>
    </div>
    
    <div class="userinput row">
            <center>
                <input type="submit" value="Save" class="btn btn-primary">
            </center>
    
    
        </div>
            </div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js">	</script>	


<script>
$(document).ready(function () {
var i=0;
$('#add_row').click(function (e) { 


i++;

$('#addnewrow').append("<tr id='removeitem"+i+"'><td data-name='details'><input type='text' name='name[]' placeholder=' Title of the paper' class='form-control'></td><td data-name='period'><input type='text' name='name1[]' placeholder='Co-author(s) if any' class='form-control'></td><td data-name='specific'><input type='text' name='name2[]' placeholder='Name of the journal / conference/publisher' class='form-control'></td><td data-name='specific'><input type='text' name='name3[]' placeholder='Vol & no' class='form-control'></td><td data-name='specific'><input type='text' name='name4[]' placeholder='Month & year' class='form-control'></td><td data-name='del'><button name='del0' class='btn btn-danger glyphicon glyphicon-remove row-remove' value='' name='del1'></button></td></tr>");

});

  
});


function Remove(id)
{


$("#removeitem"+id).remove();

}
</script>

    
    </div>
    <!-- /.box-body -->