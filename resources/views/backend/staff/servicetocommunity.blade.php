<div class="box-body">
        
		
			
<form action="{{url('staff/servicetocommunity/insert')}}" method="POST">

	{{@csrf_field()}}

		
		<div class="col-md-12 table-responsive">
				<h1><center><font color="#3c8dbc">Participation in Community Related Programmes(Skill Oriented & Awareness Programmes,Project Activities of CSC etc)</font></center></h1>
			<table class="table table-bordered table-hover table-sortable table-container col-sm-12" id="tab_logic">
				<thead>
					<tr>
						<th class="text-center col">
							Programme / Project
						</th>
						<th class="text-center col">
							Dates of participation
						</th>
						<th class="text-center col">
							Type of work executed
						</th>
    					<!-- <th class="text-center">
							Option
						</th> -->
        				<!-- <th class="text-center" style="border-top: 1px solid #ffffff; border-right: 1px solid #ffffff;"> -->
						<!-- </th> -->
					</tr>
					
				</thead>
				<tbody id="addnewrow">

					
					


					@foreach($servicetocommunity as $row)

                         
                            
					<td data-name="details">
						{{$row->Programme}}
					</td>
					<td data-name="period">
							{{$row->Dates}}
					</td>
					<td data-name="specific">
							{{$row->Type}}
					</td>

					<td data-name="del">
					<a class="btn btn-danger glyphicon glyphicon-remove row-remove" href="{{url('staff/servicetocommunity/delete/'.$row->id)}}"></a>
					</td>
				</tr>

				@endforeach





	</tbody>
				
			</table>
<a id="add_row" class="btn btn-default pull-right">Add Row</a>
		</div>
		


		<div class="userinput row">
			<center>
				<input type="submit" value="Save" class="btn btn-primary">
			</center>
	
	
		</div>
		
	
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js">	</script>	
    
    
<script>
    $(document).ready(function () {
  var i=0;
 $('#add_row').click(function (e) { 


   i++;

  $('#addnewrow').append("<tr id='add"+i+"' data-id='1'><td data-name='details'><input type='text' name='name[]' placeholder='Programme / Project' class='form-control'></td><td data-name='period'><input type='text' name='name1[]' placeholder='Dates of participation' class='form-control'></td><td data-name='specific'><input type='text' name='name2[]' placeholder='Type of work executed' class='form-control'></td><td data-name='del'><button name='del0' class='btn btn-danger glyphicon glyphicon-remove row-remove' value='' name='del1' id=i></button></td></tr>");

 });
  
      
});


function Remove(rm)
{

  $('#rm').remove();
}
	</script>
	
    
		
        </div>
        <!-- /.box-body -->