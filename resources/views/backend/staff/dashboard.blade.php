<h1><center><font color="#3c8dbc">Registration Form</font></center></h1>
<div class="box-body">
        <div class="row">
                <div class="col-lg-12">
                    
                    {{-- <h1 class="page-header text-center"><font color="#3c8dbc">Registration Form</font></h1> --}}
                    <hr>
                </div>
            </div>         
     <center>
        <div class="col-sm-3"></div>
            <div class="form-group-sm col-sm-6">
                
                <div class="userinput row" >
                    
                    <label class="col-sm-3 text-right" for="name">NAME </label> 
                    <div class="col-sm-9">
                        <input type="text" name="name" class="form-control" id="name">
                    </div>&nbsp;
                </div>
                <div class="userinput row">
                    <label class="col-sm-3 text-right">QUALIFICATION </label>
                    <div class="form-check form-check-inline col-sm-3 text-left">
                        <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="ug">
                        <label class="form-check-label" for="inlineCheckbox1">UG</label>
                    </div>
                    <div class="form-check form-check-inline col-sm-3 text-left">
                        <input class="form-check-input" type="checkbox" id="inlineCheckbox2" value="pg">
                        <label class="form-check-label" for="inlineCheckbox2">PG</label>
                    </div>
                    <div class="form-check form-check-inline col-sm-3 text-left">
                        <input class="form-check-input" type="checkbox" id="inlineCheckbox3" value="mphil" disabled>
                        <label class="form-check-label" for="inlineCheckbox3">M.Phil</label>
                    </div>
                    <div class="col-sm-3"></div>
                    <div class="form-check form-check-inline col-sm-3 text-left">
                         <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="phd">
                         <label class="form-check-label" for="inlineCheckbox1">Ph.D</label>
                    </div>
                    <div class="form-check form-check-inline col-sm-3 text-left">
                        <input class="form-check-input" type="checkbox" id="inlineCheckbox2" value="diploma">
                        <label class="form-check-label" for="inlineCheckbox2">Diploma</label>
                    </div>
                    <div class="form-check form-check-inline col-sm-3 text-left">
                        <input class="form-check-input" type="checkbox" id="inlineCheckbox3" value="iti" disabled>
                        <label class="form-check-label" for="inlineCheckbox3">ITI</label>
                    </div>
                <div>&nbsp;
                    	<div class="row">
                                <label class="col-sm-3 text-right" for="dpt">DEPARTMENT </label> 
                                                    <div class="col-sm-9">
                                                     <select class="form-control" id="dpt">
                                                         <option> select option</option>
                                                         <option>APPLIED ELECTRONICS & INSTRUMENTATION</option><br>
                                                        <OPTION>SCHOOL OF ARCHITECTURE</OPTION>
                                                        <OPTION>CIVIL ENGINEERING</OPTION>
                                                        <option>COMPUTER SCIENCE AND ENGINEERING</option>
                                                        <option>ELECTRICAL AND ELECTRONICS ENGINEERING</option>
                                                        <option>ELECTRONICS AND COMMUNICATION ENGINEERING</option>
                                                        <option>INFORMATION TECHNOLOGY</option>
                                                        <option>MECHANICAL ENGINEERING</option>
                                                        <option>MATHEMATICS</option>
                                                        <option>SCIENCE AND HUMANITIES</option>
                                                        <option>MASTER OF BUSINESS ADMINISTRATION</option>
                                                        <option>MASTER OF COMPUTER APPLICATIONS</option>
                                                     </select>
                            </div>
                                                </div>
                        </div>&nbsp;
                        <div class="row">
                                <label class="col-sm-3 text-right" for="dsgn">DESIGNATION</label> 
                                                    <div class="col-sm-9">
                                                     <select class="form-control" id="dsgn">
                                                            <option> select option</option>
                                                            <option>DIRECTOR</option>
                                                            <option>PRINCIPAL</option>
                                                            <option>VICE PRINCIPAL</option>
                                                            <option>ASSOCIATE PROFESSOR AND HEAD</option>
                                                            <option>PROFESSOR AND HEAD</option>
                                                            <option>PROFESSOR</option>
                                                            <option>ASSOCIATE PROFESSOR</option>
                                                            <option>Asst.PROFESSOR</option>
                                                            <option>LECTURER</option>
                                                            <option>GUEST LECTURER</option>
                                                            <option>INSTRUCTOR</option>
                                                            <option>WORKSHOP Supt</option>
                                                            <option>TI Gr.L</option>
                                                            <option>TRADE INSTRUCTOR</option>
                                                            <option>INSTRUCTOR Gr.L</option>
                                                            <option>LIBRARY ASSISTANT</option>
                                                            <option>INST/SM</option>
                                                            <option>PEON</option>
                                                      </select>
                                                    </div>	
                            </div>&nbsp;
                            <div class="userinput row" >
			
                                    <label class="col-sm-3 text-right" for="join">DATE OF JOINING THE INSTITUTION</label> 
                                    <div class="col-sm-9">
                                     <input type="date" name="name" class="form-control" id="join">
                                   </div>&nbsp;
                            </div>
                            <div class="col-sm-9"></div>
                            <div class="row"></div>
                            <div class="userinput row" >
			
                                <label class="col-sm-3 text-right" for="joinpr">DATE OF JOINING THE PRESENT POST</label> 
                                    <div class="col-sm-9">
                                      <input type="date" name="name" class="form-control" id="joinpr">
                                    </div>	
                               
                            </div>&nbsp;



                                    </center>  
                
            <div class="col-md-12 table-responsive">
            
                    <center>
                <button class="btn btn-primary" ><a href="{{url('staff/administrative')}}" style="color: white"> SAVE AND CONTINUE</a></button></tr>
                    </center>     
                
        
            </div>
            
            
        
       
        
            
            </div>
            <!-- /.box-body -->