@extends('backend.staff.layouts.layout')

@section('header')
@include('backend/staff/include/header')
@stop

@section('sidebar')
@include('backend/staff/include/sidebar')
@stop

@section('body')
@include('backend/staff/include/notification')
@include('backend/staff/'.$page)
@stop

@section('footer')
@include('backend/staff/include/footer')
@stop
