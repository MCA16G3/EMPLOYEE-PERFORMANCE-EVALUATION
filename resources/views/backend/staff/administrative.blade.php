

  <h1><center><font color="#3c8dbc">Administrative Work</font></center></h1>

<div class="modal fade" id="modal-default">
  
<form action="{{url('staff/administrative/insert')}}" method="post">  
  <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Add Work</h4>
        </div>
        <div class="modal-body">
          
            <div class="form-group">
                <label>Select Work</label>

                {{ csrf_field() }}

                <select class="form-control" name="work">
                  
                    <option>Select Your Work</option>

                  @foreach($Administrative_Type as $row)

                <option value="{{$row['id']}}">{{$row['workname']}}</option>
                  @endforeach
                  
                 
                </select>
              </div>


        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
          <input type="submit" class="btn btn-primary" value="Save changes">
        </div>
      </div>
      <!-- /.modal-content -->
    </div>

  </form>
    <!-- /.modal-dialog -->
  </div>
  @if(!empty($success))
  <div class="col-md-12">
      <div class="box box-default">
        <div class="box-header with-border">
          <i class="fa fa-bullhorn"></i>

          <h3 class="box-title">Callouts</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          
            @if(!empty($success))
         
          <div class="callout callout-success">
          <h4>{{$success}}</h4>


          </div>
            @endif

        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>

    @endif
  

    <!-- Main content -->
    <section class="content">
            <div class="row">

              
              <div class="col-xs-12">



                
                  <button type="button" class="btn btn-info" data-toggle="modal" data-target="#modal-default">
                     Add
                    </button>

                    
                <div class="box">
                  <div class="box-header">
                    <h3 class="box-title">Data Table With Full Features</h3>
                  </div>
                  <!-- /.box-header -->
                  <div class="box-body">
                    <table id="example1" class="table table-bordered table-striped">
                      <thead>
                      <tr>
                        <th>No</th>
                        <th>Administrative</th>
                        <th>Action</th>
                        
                      </tr>
                      </thead>
                      <tbody>
                  @php
                  $count=1;
                  @endphp
@foreach($Administrative_Work as $row)

                      <tr>
                      <td>{{$count}}</td>
                      <td>{{$row->workname}}</td>
                        
                      <td><a href="{{url('staff/administrative/delete/'.$row->workid)}}" class="">delete</a></td>
                        
                      </tr>

                      @php
                      $count++;
                      @endphp
@endforeach

                      </tbody>
                      
                    </table>
                  </div>
                  <!-- /.box-body -->
                </div>
                <!-- /.box -->
              </div>
              <!-- /.col -->
            </div>
            <!-- /.row -->
          </section>
          <!-- /.content -->