@extends('backend.layouts.layout')

@section('header')
@include('backend/include/header')
@stop

@section('sidebar')
@include('backend/include/sidebar')
@stop

@section('body')
@include('backend/'.$page)
@stop

@section('footer')
@include('backend/include/footer')
@stop
