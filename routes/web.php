<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//index

Route::get('/', 'LoginController@index');

//login

Route::get('/login', 'LoginController@login');

Route::post('/actionlogin', 'LoginController@ActionLogin');

//signup

Route::get('/signup', 'LoginController@signup');

Route::post('/actionsignup', 'LoginController@ActionSignup');



//staff

Route::get('/staff/dashboard', 'staff\DashboardController@index');

//administrative work

Route::get('staff/administrative', 'staff\AdministrativeController@index');

Route::get('staff/administrative/delete/{id}', 'staff\AdministrativeController@delete');

Route::post('staff/administrative/insert', 'staff\AdministrativeController@insert');

//academic activities 

//qualification upgradation

Route::get('staff/qualification', 'staff\academicwork\QualificationController@index');

Route::get('staff/academicwork/delete/{id}', 'staff\academicwork\QualificationController@delete');

Route::post('staff/academicwork/insert', 'staff\academicwork\QualificationController@insert');

//instructionalwork

Route::get('staff/instructionalwork', 'staff\academicwork\InstructionalWorkController@index');

Route::get('staff/instructionalwork/delete/{id}', 'staff\academicwork\InstructionalWorkController@delete');

Route::post('staff/instructionalwork/insert', 'staff\academicwork\InstructionalWorkController@insert');

//participation

Route::get('staff/participation', 'staff\academicwork\ParticipationController@index');

Route::get('staff/participation/delete/{id}', 'staff\academicwork\ParticipationController@delete');

Route::post('staff/participation/insert', 'staff\academicwork\ParticipationController@insert');

//activitiesorganized

Route::get('staff/activitiesorganized', 'staff\academicwork\ActivitiesorganizdController@index');

Route::get('staff/activitiesorganized/delete/{id}', 'staff\academicwork\ActivitiesorganizdController@delete');

Route::post('staff/activitiesorganized/insert', 'staff\academicwork\ActivitiesorganizdController@insert');

//industrialtraining

Route::get('staff/industrialtraining', 'staff\academicwork\IndustrialtrainingController@index');

Route::get('staff/industrialtraining/delete/{id}', 'staff\academicwork\IndustrialtrainingController@delete');

Route::post('staff/industrialtraining/insert', 'staff\academicwork\IndustrialtrainingController@insert');


//Curricular

Route::get('staff/Curricular', 'staff\academicwork\CurricularController@index');

Route::get('staff/Curricular/delete/{id}', 'staff\academicwork\CurricularController@delete');

Route::post('staff/Curricular/insert', 'staff\academicwork\CurricularController@insert');


//extracurricular

Route::get('staff/extracurricular', 'staff\academicwork\ExtracurricularController@index');

Route::get('staff/extracurricular/delete/{id}', 'staff\academicwork\ExtracurricularController@delete');

Route::post('staff/extracurricular/insert', 'staff\academicwork\ExtracurricularController@insert');

//supervisorywork

Route::get('staff/supervisorywork', 'staff\academicwork\SupervisoryworkController@index');

Route::get('staff/supervisorywork/delete/{id}', 'staff\academicwork\SupervisoryworkController@delete');

Route::post('staff/supervisorywork/insert', 'staff\academicwork\SupervisoryworkController@insert');

//speciallectures

Route::get('staff/speciallectures', 'staff\academicwork\SpeciallecturesController@index');

Route::get('staff/speciallectures/delete/{id}', 'staff\academicwork\SpeciallecturesController@delete');

Route::post('staff/speciallectures/insert', 'staff\academicwork\SpeciallecturesController@insert');

//research


//development

Route::get('staff/development', 'staff\research\DevelopmentController@index');

Route::get('staff/development/delete/{id}', 'staff\research\DevelopmentController@delete');

Route::post('staff/development/insert', 'staff\research\DevelopmentController@insert');

//fundgeneration

Route::get('staff/fundgeneration', 'staff\research\FundgenerationController@index');

Route::get('staff/fundgeneration/delete/{id}', 'staff\research\FundgenerationController@delete');

Route::post('staff/fundgeneration/insert', 'staff\research\FundgenerationController@insert');

//researchguidance

Route::get('staff/researchguidance', 'staff\research\ResearchguidanceController@index');

Route::get('staff/researchguidance/delete/{id}', 'staff\research\ResearchguidanceController@delete');

Route::post('staff/researchguidance/insert', 'staff\research\ResearchguidanceController@insert');

//research papers

Route::get('staff/researchpapers', 'staff\research\ResearchpapersController@index');

Route::get('staff/researchpapers/delete/{id}', 'staff\research\ResearchpapersController@delete');

Route::post('staff/researchpapers/insert', 'staff\research\ResearchpapersController@insert');

//publication of books

Route::get('staff/publicationofbooks', 'staff\research\PublicationofbooksController@index');

Route::get('staff/publicationofbooks/delete/{id}', 'staff\research\PublicationofbooksController@delete');

Route::post('staff/publicationofbooks/insert', 'staff\research\PublicationofbooksController@insert');

//

Route::get('staff/professionalsociety', 'staff\research\ProfessionalsocietyControllerer@index');

Route::get('staff/achievements', 'staff\research\AchievementsControllerer@index');

//service to community

Route::get('staff/servicetocommunity', 'staff\ServicetocommunityController@index');

//other activities

Route::get('staff/otheractivities', 'staff\OtheractivitiesController@index');




