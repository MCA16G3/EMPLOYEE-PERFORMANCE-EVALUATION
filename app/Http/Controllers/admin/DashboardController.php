<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    

     public function index()
     {
 
 
         
         $data['username']=session()->get('username');
 
 
          $data['page']='dashboard';
 
 
         return view('backend/admin/template',$data);
     }
 


}
