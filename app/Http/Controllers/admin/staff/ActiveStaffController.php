<?php

namespace App\Http\Controllers\admin\staff;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\model\tbl_users;
use DB;


class ActiveStaffController extends Controller
{
    

    public function index()
    {

        $data['username']=session()->get('username');

        $data['staff'] = DB::table('tbl_users')->where('fk_status_id',1)->get()->toarray();

        $data['page']='staff/active';
     
        return view('backend/admin/template',$data);
    }

    
    public function update($username)
    {


        DB::table('tbl_users')
            ->where('username', $username)
            ->update(['fk_status_id' =>2]);


         return redirect('admin/staff/active');

    }

}
