<?php

namespace App\Http\Controllers\admin\staff;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\model\tbl_users;
use DB;


class StaffController extends Controller
{
    //



    public function index()
    {

        $data['username']=session()->get('username');

        $data['staff'] = DB::table('tbl_users')->get()->toarray();

        $data['page']='staff/list';
     
        return view('backend/admin/template',$data);
    }

  

}
