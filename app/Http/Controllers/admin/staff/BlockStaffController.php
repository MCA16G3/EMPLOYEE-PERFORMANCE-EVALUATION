<?php

namespace App\Http\Controllers\admin\staff;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\model\tbl_users;
use DB;


class BlockStaffController extends Controller
{


    public function index()
    {

        $data['username']=session()->get('username');

        $data['staff'] = DB::table('tbl_users')->where('fk_status_id',2)->get()->toarray();

        $data['page']='staff/block';
     
        return view('backend/admin/template',$data);
    }

    
    public function update($username)
    {


        DB::table('tbl_users')
            ->where('username', $username)
            ->update(['fk_status_id' =>1]);




            return redirect('admin/staff/block');            //  return redirect('staff/activitiesorganized');

    }




    //
}
