<?php

namespace App\Http\Controllers;
use DB;
use Illuminate\Http\Request;
use App\model\tbl_user;
use App\model\tbl_signup;
use App\model\tbl_user_detail;
use Illuminate\Support\Facades\Validator;

class LoginController extends Controller
{
    

public function index()
{

return view('frontend.include.index');

}

  public function login()
  {

    return view('backend/login');

  }
  public function signup()
  {

    return view('frontend/signup');

  }

  public function ActionSignup(Request $request)
  {
  

    $validator = Validator::make($request->all(), [
      'email' => 'email|unique:tbl_signups',
      'phone' => 'required|min:10|numeric',
      'password' => 'required',
      'cpassword' => 'required|same:password'
   ]);

   if ($validator->fails()) 
   {
       // redirect to login page
      return redirect('signup')
      ->withErrors($validator); //its contain validation messages 
     
  }
  else
   {


$tbl_userlogin=new tbl_user();

$tbl_userlogin->username=$request->uid;

$tbl_userlogin->password=$request->password;

$tbl_userlogin->fk_user_type_id=2; //WHO IS CREATE THIS THAT PERSON

$tbl_userlogin->fk_status_id=1; //ACTIVE MODE

$tbl_userlogin->save();

//OTHER DETAILS SAVE TO TBL SIGNUP

$tbl_signup=new tbl_signup();

$tbl_signup->first_name=$request->fname;

$tbl_signup->email=$request->email;

$tbl_signup->phone_no=$request->phone;

$tbl_signup->gender=$request->gender;

$tbl_signup->fk_user_id=$tbl_userlogin->id;

if($tbl_signup->save())
  {


    $tbl_user_details=new tbl_user_detail();
    
    $tbl_user_details->fk_user_id=$tbl_signup->id;

    $tbl_user_details->save();

     // echo $tbl_signup->id;

    return redirect('login');

  }
   }
  }

  public function ActionLogin(Request $request)
    {

 
     $request->userid;

     $validatedData = validator::make($request->all(),[
      'userid' => 'required|max:10',
      'pass' => 'required',
     ]);

        if ($validatedData->fails()) 
        {
           
          return view('login')->withErrors($validatedData);        }
        else
        {
         
        $value =tbl_user::where('username', $request->userid)->get()->toarray(); //array
          
         
        if(count($value)==1)
        {

          if($value[0]['password'] == $request->pass)
          {
           
           //admin status is 1

           if($value[0]['fk_status_id']==1)
             {
                $login_details=array(
                  'username'=>$request->userid,
                  'password'=>$request->pass,
                  'user_id'=>$value[0]['Id'],
                  'status'=>$value[0]['fk_status_id']
                );

                $request->session()->put($login_details);

                  if($value[0]['fk_user_type_id']==1)
                  {
die();
                  }
                  elseif($value[0]['fk_user_type_id']==2)
                  {


                   return redirect('staff/dashboard');

                  }
                  


            

            
             }
            

          }
          else
          {
            echo 'invalid username and password';
          }
         

        }



          
        }
    }




}
