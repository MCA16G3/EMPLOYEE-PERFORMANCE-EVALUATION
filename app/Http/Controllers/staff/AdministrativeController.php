<?php

namespace App\Http\Controllers\staff;
use App\model\tbl_administrative_type;
use App\model\tbl_administrative_work;
use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class AdministrativeController extends Controller
{
    

    public function index($data=null)
    {



         $data['Administrative_Type']=tbl_administrative_type::all()->toarray();

         $data['Administrative_Work']=DB::table('tbl_administrative_works')->select('tbl_administrative_works.id as workid','tbl_administrative_types.workname as workname')->join('tbl_administrative_types', 'tbl_administrative_types.id', '=', 'tbl_administrative_works.fk_administrative_type_id')->where('fk_user_id', session()->get('user_id'))->get()->toarray();
         


//          print_R($data['Administrative_Work']);

// die(); 
        $data['page']='administrative';
         

         return view('backend/staff/template',$data);
    }


    public function insert(Request $request)
    {
        

        $validatedData = validator::make($request->all(),[
            'work' => 'required|integer',
           ]);
      
              if ($validatedData->fails()) 
              {
                 
                return view('login')->withErrors($validatedData);       
            
               }
               else
               {

                $tbl_administrative_work=new tbl_administrative_work();
      
                $tbl_administrative_work->fk_administrative_type_id=$request->work;

                $tbl_administrative_work->fk_user_id =$request->session()->get('user_id');

                $tbl_administrative_work->fk_status_id  =1;

                if($tbl_administrative_work->save())
                {
                
                   $data['success']="successfully Created";    
                
                   return $this->index($data);
                
                 }
                else
                {
                    echo 'database error';
                    die();
                }
               }

    }



    public function delete($id)
    {


         $tbl_administrative_work = tbl_administrative_work::find($id);
        
        if($tbl_administrative_work->delete())
        {

           // echo  "delete";

            session()->flash('success', 'Task was successful!');

           return redirect('staff/administrative');
        }
    }
}
