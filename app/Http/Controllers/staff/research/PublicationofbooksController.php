<?php

namespace App\Http\Controllers\staff\research;
use DB;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\model\tbl_pub_books;

class PublicationofbooksController extends Controller
{
    //
    public function index()
    {

        $data['publicationofbooks'] = DB::table('tbl_pub_books')->where('fk_user_id', session()->get('user_id'))->get()->toarray();



        $data['page']='research/publicationofbooks';
        return view('backend/staff/template',$data);
    }

    public function insert(Request $request)
   {

    $tbl_pub_books=new tbl_pub_books();
      
  foreach($_POST['name'] as $key=>$row)
  {
  
    $tbl_pub_books->ttl_bks=$_POST['name'][$key];
    
    $tbl_pub_books->co_auth=$_POST['name1'][$key];

    
    $tbl_pub_books->publisher=$_POST['name2'][$key];

    
    $tbl_pub_books->mnth_yr=$_POST['name3'][$key];

    
    $tbl_pub_books->price =$_POST['name4'][$key];

    
    $tbl_pub_books->fk_user_id =$request->session()->get('user_id');

   

    if($tbl_pub_books->save())
    {

      
        session()->flash('success', 'Task was successful!');

        
        return redirect('staff/publicationofbooks');
     }
    }
}



        public function delete($id)
        {
         
            $tbl_pub_books = tbl_pub_books::find($id);
        
            if($tbl_pub_books->delete())
            {
    
    
                session()->flash('success', 'Task was successful!');
    
               return redirect('staff/publicationofbooks');
            }

        }
}


