<?php

namespace App\Http\Controllers\staff\research;
use DB;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\model\tbl_fund_gens;

class FundgenerationController extends Controller
{
    //
    public function index()
    {

        $data['fundgeneration'] = DB::table('tbl_fund_gens')->where('fk_user_id', session()->get('user_id'))->get()->toarray();
      
        $data['page']='research/fundgeneration';
        return view('backend/staff/template',$data);
    
    
    }
    public function insert(Request $request)
    {
 
     $tbl_fund_gens=new tbl_fund_gens();
       
   foreach($_POST['name'] as $key=>$row)
   {
   
     $tbl_fund_gens->nat_actvty=$_POST['name'][$key];
     
     $tbl_fund_gens->conc_agncy=$_POST['name1'][$key];
 
     
     $tbl_fund_gens->fund_gen=$_POST['name2'][$key];

    $tbl_fund_gens->act_crnt_sts=$_POST['name3'][$key];
     
     
     $tbl_fund_gens->fk_user_id =$request->session()->get('user_id');
 
    
 
     if($tbl_fund_gens->save())
     {
 
       
         session()->flash('success', 'Task was successful!');
 
         
         return redirect('staff/fundgeneration');
      }
     }
 }
 
 
 
         public function delete($id)
         {
          
             $tbl_fund_gens = tbl_fund_gens::find($id);
         
             if($tbl_fund_gens->delete())
             {
     
                // echo  "delete";
     
                 session()->flash('success', 'Task was successful!');
     
                return redirect('staff/fundgeneration');
             }
 
         }
 
 }
 