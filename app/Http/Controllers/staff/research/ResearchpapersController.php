<?php

namespace App\Http\Controllers\staff\research;

use DB;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\model\tbl_rsch_papers;

class ResearchpapersController extends Controller
{
    //
    public function index()
    {

        $data['researchpapers'] = DB::table('tbl_rsch_papers')->where('fk_user_id', session()->get('user_id'))->get()->toarray();
       
        $data['page']='research/researchpapers';
        return view('backend/staff/template',$data);
    }

    public function insert(Request $request)
   {

    $tbl_rsch_papers=new tbl_rsch_papers();
      
  foreach($_POST['name'] as $key=>$row)
  {
  
    $tbl_rsch_papers->tlt_ppr=$_POST['name'][$key];
    
    $tbl_rsch_papers->co_auth=$_POST['name1'][$key];

    
    $tbl_rsch_papers->name_jrnl=$_POST['name2'][$key];

    
    $tbl_rsch_papers->vol_no =$_POST['name3'][$key];

    
    $tbl_rsch_papers->mnth_yr=$_POST['name4'][$key];

    
   $tbl_rsch_papers->fk_user_id =$request->session()->get('user_id');

   

    if($tbl_rsch_papers->save())
    {

      
        session()->flash('success', 'Task was successful!');

        
        return redirect('staff/researchpapers');
     }
    }
}



        public function delete($id)
        {
         
            $tbl_rsch_papers = tbl_rsch_papers::find($id);
        
            if($tbl_rsch_papers->delete())
            {
    
               
    
                session()->flash('success', 'Task was successful!');
    
               return redirect('staff/researchpapers');
            }

        }

}
