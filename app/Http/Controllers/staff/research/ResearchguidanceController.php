<?php

namespace App\Http\Controllers\staff\research;
use DB;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\model\tbl_rsch_guidences;

class ResearchguidanceController extends Controller
{
    //
    public function index()
    {

        $data['researchguidance'] = DB::table('tbl_rsch_guidences')->where('fk_user_id', session()->get('user_id'))->get()->toarray();

        $data['page']='research/researchguidance';
        return view('backend/staff/template',$data);
    }


    public function insert(Request $request)
    {
 
     $tbl_rsch_guidences=new tbl_rsch_guidences();
       
   foreach($_POST['name'] as $key=>$row)
   {
   
     $tbl_rsch_guidences->ms_mphil=$_POST['name'][$key];
     
     $tbl_rsch_guidences->name_std =$_POST['name1'][$key];
 
     
     $tbl_rsch_guidences->reg_date =$_POST['name2'][$key];
 
     
     $tbl_rsch_guidences->ft_pt=$_POST['name3'][$key];

     $tbl_rsch_guidences->co_guide =$_POST['name4'][$key];

     $tbl_rsch_guidences->crt_sts =$_POST['name5'][$key];
 
     
 
 
     $tbl_rsch_guidences->fk_user_id =$request->session()->get('user_id');
 
    
 
     if($tbl_rsch_guidences->save())
     {
 
       
         session()->flash('success', 'successfully inserted!');
 
         
         return redirect('staff/researchguidance');
      }
     }
 }
 
 
 
         public function delete($id)
         {
          
             $tbl_rsch_guidences = tbl_rsch_guidences::find($id);
         
             if($tbl_rsch_guidences->delete())
             {
     
                // echo  "delete";
     
                 session()->flash('success', 'successfully deleted!');
     
                return redirect('staff/researchguidance');
             }
 
         }
}
