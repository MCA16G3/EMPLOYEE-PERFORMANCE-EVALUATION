<?php

namespace App\Http\Controllers\staff\research;

use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\model\tbl_dev_dpts;

class DevelopmentController extends Controller
{
    //
    public function index()
    {

        $data['development'] = DB::table('tbl_dev_dpts')->where('fk_user_id', session()->get('user_id'))->get()->toarray();

        $data['page']='research/development';
        return view('backend/staff/template',$data);
    }

    public function insert(Request $request)
   {

    $tbl_dev_dpts=new tbl_dev_dpts();
      
  foreach($_POST['name'] as $key=>$row)
  {
  
    $tbl_dev_dpts->det_act=$_POST['name'][$key];
    
    $tbl_dev_dpts->nat_wrk=$_POST['name1'][$key];

    
    $tbl_dev_dpts->crnt_sts=$_POST['name2'][$key];

    
    $tbl_dev_dpts->fk_user_id =$request->session()->get('user_id');

   

    if($tbl_dev_dpts->save())
    {

      
        session()->flash('success', 'Task was successful!');

        
        return redirect('staff/development');
     }
    }
}



        public function delete($id)
        {
         
            $tbl_dev_dpts = tbl_dev_dpts::find($id);
        
            if($tbl_dev_dpts->delete())
            {
    
               // echo  "delete";
    
                session()->flash('success', 'Task was successful!');
    
               return redirect('staff/development');
            }

        }

}
