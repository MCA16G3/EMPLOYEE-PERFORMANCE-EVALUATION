<?php


namespace App\Http\Controllers\staff\academicwork;
use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\model\tbl_spc_lects;

class SpeciallecturesController extends Controller
{
    //
    public function index()
    {

        $data['speciallectures'] = DB::table('tbl_spc_lects')->where('fk_user_id', session()->get('user_id'))->get()->toarray();

        $data['page']='academicwork/speciallectures';
        return view('backend/staff/template',$data);
    }

    public function insert(Request $request)
   {

    $tbl_spc_lects=new tbl_spc_lects();
      
  foreach($_POST['name'] as $key=>$row)
  {
  
    $tbl_spc_lects->dates=$_POST['name'][$key];
    
    $tbl_spc_lects->no_of_hrs =$_POST['name1'][$key];

    
    $tbl_spc_lects->org_insti=$_POST['name2'][$key];

    
    $tbl_spc_lects->type_prgm=$_POST['name3'][$key];

    
    


    $tbl_spc_lects->fk_user_id =$request->session()->get('user_id');

   

    if($tbl_spc_lects->save())
    {

      
        session()->flash('success', 'Task was successful!');

        
        return redirect('staff/speciallectures');
     }
    }
}



        public function delete($id)
        {
         
            $tbl_spc_lects = tbl_spc_lects::find($id);
        
            if($tbl_spc_lects->delete())
            {
    
               // echo  "delete";
    
                session()->flash('success', 'Task was successful!');
    
               return redirect('staff/speciallectures');
            }

        }
}
