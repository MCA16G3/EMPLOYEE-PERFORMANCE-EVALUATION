<?php


namespace App\Http\Controllers\staff\academicwork;

use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\model\tbl_sup_wrks;

class SupervisoryworkController extends Controller
{
    //
    public function index()
    {

        $data['supervisorywork'] = DB::table('tbl_sup_wrks')->where('fk_user_id', session()->get('user_id'))->get()->toarray();

        $data['page']='academicwork/supervisorywork';
        return view('backend/staff/template',$data);
    }

    public function insert(Request $request)
   {

    $tbl_sup_wrks=new tbl_sup_wrks();
      
  foreach($_POST['name'] as $key=>$row)
  {
  
    $tbl_sup_wrks->degree=$_POST['name'][$key];
    
    $tbl_sup_wrks->name_std=$_POST['name1'][$key];

    
    $tbl_sup_wrks->co_guide=$_POST['name2'][$key];

    
    $tbl_sup_wrks->ttl_prj=$_POST['name3'][$key];

    
    $tbl_sup_wrks->cur_sts=$_POST['name4'][$key];

   

    

    $tbl_sup_wrks->fk_user_id =$request->session()->get('user_id');

   

    if($tbl_sup_wrks->save())
    {

      
        session()->flash('success', 'successfully inserted!');

        
        return redirect('staff/supervisorywork');
     }
    }
}



        public function delete($id)
        {
         
            $tbl_sup_wrks = tbl_sup_wrks::find($id);
        
            if($tbl_sup_wrks->delete())
            {
    
               // echo  "delete";
    
                session()->flash('success', 'successfully deleted!');
    
               return redirect('staff/supervisorywork');
            }

        }
}
