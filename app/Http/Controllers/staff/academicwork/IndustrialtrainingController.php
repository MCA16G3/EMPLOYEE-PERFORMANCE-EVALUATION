<?php

namespace App\Http\Controllers\staff\academicwork;

use DB;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\model\tbl_ind_trainings;

class IndustrialtrainingController extends Controller
{
    //
    public function index()
    {

        $data['industrialtraining'] = DB::table('tbl_ind_trainings')->where('fk_user_id', session()->get('user_id'))->get()->toarray();

        $data['page']='academicwork/industrialtraining';
        return view('backend/staff/template',$data);
    }

    public function insert(Request $request)
   {

    $tbl_ind_trainings=new tbl_ind_trainings();
      
  foreach($_POST['name'] as $key=>$row)
  {
  
    $tbl_ind_trainings->actvty=$_POST['name'][$key];
    
    $tbl_ind_trainings->title_act =$_POST['name1'][$key];

    
    $tbl_ind_trainings->dur_from=$_POST['name2'][$key];

    
    $tbl_ind_trainings->dur_to=$_POST['name3'][$key];

    
    $tbl_ind_trainings->loc_nat=$_POST['name4'][$key];

    
    $tbl_ind_trainings->no_partici=$_POST['name5'][$key];

    
    $tbl_ind_trainings->sponsrs=$_POST['name6'][$key];

   


    $tbl_ind_trainings->fk_user_id =$request->session()->get('user_id');

   

    if($tbl_ind_trainings->save())
    {

      
        session()->flash('success', 'successfully inserted!');

        
        return redirect('staff/industrialtraining');
     }
    }
}



        public function delete($id)
        {
         
            $tbl_ind_trainings = tbl_ind_trainings::find($id);
        
            if($tbl_ind_trainings->delete())
            {
    
               // echo  "delete";
    
                session()->flash('success', 'successfully deleted!');
    
               return redirect('staff/industrialtraining');
            }

        }
}



