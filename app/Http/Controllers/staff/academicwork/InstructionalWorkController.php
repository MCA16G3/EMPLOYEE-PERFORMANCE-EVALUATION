<?php

namespace App\Http\Controllers\staff\academicwork;
use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\model\tbl_instructionalworks;

class InstructionalWorkController extends Controller
{
    //

    public function index()
    {




        $data['instructionalwork'] = DB::table('tbl_instructionalworks')->where('fk_user_id', session()->get('user_id'))->get()->toarray();
      


        $data['page']='academicwork/InstructionalWork';
        
        return view('backend/staff/template',$data);
    }

public function insert(Request $request)
   {

    $tbl_instructionalworks=new tbl_instructionalworks();
      
  foreach($_POST['name'] as $key=>$row)
  {
  
    $tbl_instructionalworks->sem_prg=$_POST['name'][$key];
    
    $tbl_instructionalworks->cr_title =$_POST['name1'][$key];

    
    $tbl_instructionalworks->L=$_POST['name2'][$key];

    
    $tbl_instructionalworks->T=$_POST['name3'][$key];

    
    $tbl_instructionalworks->P=$_POST['name4'][$key];

    
    $tbl_instructionalworks->class=$_POST['name5'][$key];

    
    $tbl_instructionalworks->overallavg=$_POST['name6'][$key];

    $tbl_instructionalworks->numbergot=$_POST['name7'][$key];


    $tbl_instructionalworks->exampass=$_POST['name8'][$key];


    $tbl_instructionalworks->fk_user_id =$request->session()->get('user_id');

   

    if($tbl_instructionalworks->save())
    {

      
        session()->flash('success', 'Task was successful!');

        
        return redirect('staff/instructionalwork');
     }
    }
}



        public function delete($id)
        {
         
            $tbl_instructionalworks = tbl_instructionalworks::find($id);
        
            if($tbl_instructionalworks->delete())
            {
    
               // echo  "delete";
    
                session()->flash('success', 'Task was successful!');
    
               return redirect('staff/instructionalwork');
            }

        }

}
