<?php

namespace App\Http\Controllers\staff\academicwork;

use DB;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\model\tbl_dtl_qualifications;

class QualificationController extends Controller
{
    //
    public function index()
    {

        $data['academicwork'] = DB::table('tbl_dtl_qualifications')->where('fk_user_id', session()->get('user_id'))->get()->toarray();

        $data['page']='academicwork/academicwork';
        return view('backend/staff/template',$data);
    }

    public function insert(Request $request)
    {
 
     $tbl_dtl_qualifications=new tbl_dtl_qualifications();
       
   foreach($_POST['name'] as $key=>$row)
   {
   
     $tbl_dtl_qualifications->degree=$_POST['name'][$key];
     
     $tbl_dtl_qualifications->ft_pt_dist=$_POST['name1'][$key];
 
     
     $tbl_dtl_qualifications->clg_uni=$_POST['name2'][$key];
 
     
     $tbl_dtl_qualifications->date_join=$_POST['name3'][$key];
 
     
     $tbl_dtl_qualifications->cur_sts=$_POST['name4'][$key];
 
     
    
 
 
     $tbl_dtl_qualifications->fk_user_id =$request->session()->get('user_id');
 
    
 
     if($tbl_dtl_qualifications->save())
     {
 
       
         session()->flash('success', 'Task was successful!');
 
         
         return redirect('staff/qualification');
      }
     }
 }
 
 
 
         public function delete($id)
         {
          
             $tbl_dtl_qualifications = tbl_dtl_qualifications::find($id);
         
             if($tbl_dtl_qualifications->delete())
             {
     
                // echo  "delete";
     
                 session()->flash('success', 'Task was successful!');
     
                return redirect('staff/qualification');
             }
 
         }
 
 }