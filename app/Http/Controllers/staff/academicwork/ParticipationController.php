<?php
namespace App\Http\Controllers\staff\academicwork;
use DB;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\model\tbl_participations;


class ParticipationController extends Controller
{
    //
    public function index()
    {

        $data['participation'] = DB::table('tbl_participations')->where('fk_user_id', session()->get('user_id'))->get()->toarray();

        $data['page']='academicwork/participation';
        return view('backend/staff/template',$data);
    }

    public function insert(Request $request)
   {

    $tbl_participations=new tbl_participations();
      
  foreach($_POST['name'] as $key=>$row)
  {
  
    $tbl_participations->nature_activity=$_POST['name'][$key];
    
    $tbl_participations->org_by =$_POST['name1'][$key];

    
    $tbl_participations->dur_from=$_POST['name2'][$key];

    
    $tbl_participations->dur_to=$_POST['name3'][$key];

    
   $tbl_participations->fk_user_id =$request->session()->get('user_id');

   

    if($tbl_participations->save())
    {

      
        session()->flash('success', 'successfully inserted!');

        
        return redirect('staff/participation');
     }
    }
}



        public function delete($id)
        {
         
            $tbl_participations = tbl_participations::find($id);
        
            if($tbl_participations->delete())
            {
    
               // echo  "delete";
    
                session()->flash('success', 'successfully deleted!');
    
               return redirect('staff/participation');
            }

        }

}

