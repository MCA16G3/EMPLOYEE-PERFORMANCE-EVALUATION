<?php

namespace App\Http\Controllers\staff\academicwork;
use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\model\tbl_activity_orgs;

class ActivitiesorganizdController extends Controller
{
    //
    public function index()
    {

        $data['activitiesorganized'] = DB::table('tbl_activity_orgs')->where('fk_user_id', session()->get('user_id'))->get()->toarray();

        $data['page']='academicwork/activitiesorganized';
        return view('backend/staff/template',$data);
    }

    public function insert(Request $request)
   {

    $tbl_activity_orgs=new tbl_activity_orgs();
      
  foreach($_POST['name'] as $key=>$row)
  {
  
    $tbl_activity_orgs->activity=$_POST['name'][$key];
    
    $tbl_activity_orgs->title_activity=$_POST['name1'][$key];

    
    $tbl_activity_orgs->dur_from=$_POST['name2'][$key];

    
    $tbl_activity_orgs->dur_to=$_POST['name3'][$key];

    
    $tbl_activity_orgs->loc_nat_int=$_POST['name4'][$key];

    
    $tbl_activity_orgs->no_part=$_POST['name5'][$key];

    
    $tbl_activity_orgs->spons=$_POST['name6'][$key];

   

    $tbl_activity_orgs->fk_user_id =$request->session()->get('user_id');

   

    if($tbl_activity_orgs->save())
    {

      
        session()->flash('success', 'Task was successful!');

        
        return redirect('staff/activitiesorganized');
     }
    }
}



        public function delete($id)
        {
         
            $tbl_activity_orgs = tbl_activity_orgs::find($id);
        
            if($tbl_activity_orgs->delete())
            {
    
               // echo  "delete";
    
                session()->flash('success', 'Task was successful!');
    
               return redirect('staff/activitiesorganized');
            }

        }

}


