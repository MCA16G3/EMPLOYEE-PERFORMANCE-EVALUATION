<?php

namespace App\Http\Controllers\staff\academicwork;

use DB;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\model\tbl_extra_curis;

class ExtracurricularController extends Controller
{
    //
    
    public function index()
    {

        $data['extracurricular'] = DB::table('tbl_extra_curis')->where('fk_user_id', session()->get('user_id'))->get()->toarray();

        $data['page']='academicwork/extracurricular';
        return view('backend/staff/template',$data);
    }

    public function insert(Request $request)
   {

    $tbl_extra_curis=new tbl_extra_curis();
      
  foreach($_POST['name'] as $key=>$row)
  {
  
    $tbl_extra_curis->dtls_pos_hld=$_POST['name'][$key];
    
    $tbl_extra_curis->period =$_POST['name1'][$key];

    
    $tbl_extra_curis->spc_ach=$_POST['name2'][$key];

    
 


    $tbl_extra_curis->fk_user_id =$request->session()->get('user_id');

   

    if($tbl_extra_curis->save())
    {

      
        session()->flash('success', 'Task was successful!');

        
        return redirect('staff/extracurricular');
     }
    }
}



public function delete($id)
{
 
    $tbl_extra_curis = tbl_extra_curis::find($id);

    if($tbl_extra_curis->delete())
    {

       // echo  "delete";

        session()->flash('success', 'Task was successful!');

       return redirect('staff/extracurricular');
    }

}

}


